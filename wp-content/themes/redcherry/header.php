<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package RedCherrry
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'redcherry' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-2">
                        <?php get_template_part( 'template-parts/header/site', 'branding' ); ?>
                    </div>
                    <div class="col-md-10">
                        <?php get_template_part( 'template-parts/navigation/social', 'top' ); ?>
                        <?php get_template_part( 'template-parts/navigation/contact', 'top' ); ?>
                    </div>
                </div>
            </div>
	</header><!-- #masthead -->
        <div class="navigation_container">
            <div class="container">
                <?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
            </div>
        </div>
<?php 
    if (is_home() || is_front_page()) {
        echo do_shortcode("[metaslider id=39]");
    }
?>        

	<div id="content" class="site-content">
            <div class="container">
            <?php
            redcherry_core()->init_module( 'cherry-breadcrumbs', 
                    array(
                        'wrapper_format' => '<div class="wrap"><div class="title-row">%1$s</div><div class="items-row">%2$s</div></div>',
                        'show_title' => false,
                        'show_on_front' => false,
                        'show_browse' => false,
                        ) 
                    );
            do_action('cherry_breadcrumbs_render');
            ?>
