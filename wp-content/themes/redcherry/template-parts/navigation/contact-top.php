<?php
/* Copyright 2016 KamilNowak.com */
?>
<?php wp_nav_menu( array(
    'theme_location' => 'contact',
    'menu_id'        => 'contact-top',
    'menu_class'     => 'contact-links-menu menu-icons',
    'depth'          => 1,
    'walker'         => new recherry_icon_walker()
) ); ?>
<!-- #social-top -->