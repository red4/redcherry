<?php
/* Copyright 2016 KamilNowak.com */
?>
<?php wp_nav_menu( array(
    'theme_location' => 'social',
    'menu_id'        => 'social-top',
    'menu_class'     => 'social-links-menu menu-icons',
    'depth'          => 1,
    'walker'         => new recherry_icon_walker()
) ); ?>
<!-- #social-top -->