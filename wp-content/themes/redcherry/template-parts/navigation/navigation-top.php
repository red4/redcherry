<?php
/* Copyright 2016 KamilNowak.com */
?>
<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php _e( 'Top Menu', 'redcherry' ); ?>">
	<button class="menu-toggle" aria-controls="top-menu" aria-expanded="false">Menu</button>
	<?php wp_nav_menu( array(
		'theme_location' => 'top',
		'menu_id'        => 'top-menu',
	) ); ?>
</nav><!-- #site-navigation -->