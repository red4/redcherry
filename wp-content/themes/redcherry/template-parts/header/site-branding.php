<div class="site-branding">
    <a class="logo" href="<?php echo esc_url(home_url('/')); ?>">
        <img src="<?php echo get_template_directory_uri(); ?>/logo.png" rel="home" alt="<?php bloginfo('name'); ?>" />
    </a>
</div><!-- .site-branding -->