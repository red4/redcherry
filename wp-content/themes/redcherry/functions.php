<?php

/**
 * Load setup file for Cherry V
 */
add_action( 'after_setup_theme', require( get_template_directory() . '/cherry-framework/setup.php' ), 0 );

/**
 * Load Cherry V core
 */
add_action( 'after_setup_theme', 'redcherry_core', 1 );
function redcherry_core() {
    global $chery_core_version;
    static $core = null;
    if ( null !== $core ) {
        return $core;
    }
    if ( 0 < sizeof( $chery_core_version ) ) {
        $core_paths = array_values( $chery_core_version );
        require_once( $core_paths[0] );
    } else {
        die( 'Class Cherry_Core not found' );
    }
    $core = new Cherry_Core( array(
        'base_dir' => get_template_directory() . '/cherry-framework',
        'base_url' => get_template_directory_uri() . '/cherry-framework',
        'modules'  => array(
            'cherry-js-core' => array(
                'autoload' => true,
            ),
            'cherry-ui-elements' => array(
                'autoload' => false,
            ),
            'cherry-interface-builder' => array(
                'autoload' => false,
            ),
            'cherry-widget-factory' => array(
                'autoload' => true,
            ),
            'cherry-customizer' => array(
                'autoload' => false,
            ),
            'cherry-dynamic-css' => array(
                'autoload' => false,
            ),
            'cherry-google-fonts-loader' => array(
                'autoload' => false,
            ),
            'cherry-term-meta' => array(
                'autoload' => false,
            ),
            'cherry-post-meta' => array(
                'autoload' => false,
            ),
            'cherry-breadcrumbs' => array(
                'autoload' => false,
            ),
        ),
    ) );
 
    return $core;
}

/**
 * RedCherrry functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package RedCherrry
 */

if ( ! function_exists( 'redcherry_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function redcherry_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on RedCherrry, use a find and replace
	 * to change 'redcherry' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'redcherry', get_template_directory() . '/languages' );
        
        register_nav_menus(array(
            'top' => __('Top Menu', 'redcherry'),
            'social' => __('Social Links Menu', 'redcherry'),
            'contact' => __('Contact Links Menu', 'redcherry'),
        ));

        add_theme_support( 'starter-content', 
            array(
                'nav_menus' => array(
                    'top' => array(
                        'name' => __( 'Top Menu', 'redcherry' ),
                        'items' => array(
                            'page_home',
                            'page_about',
                            'page_blog',
                            'page_contact',
                        ),
                    ),
                    'social' => array(
                        'name' => __( 'Social Links Menu', 'redcherry' ),
                        'items' => array(
                            'link_yelp',
                            'link_facebook',
                            'link_twitter',
                            'link_instagram',
                            'link_email',
                        ),
                    ),
                    'contact' => array(
                        'name' => __( 'Contact Links Menu', 'redcherry'),
                    ),
                ),
            )
        );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'redcherry' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'redcherry_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'redcherry_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function redcherry_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'redcherry_content_width', 640 );
}
add_action( 'after_setup_theme', 'redcherry_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function redcherry_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'redcherry' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'redcherry' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'redcherry_widgets_init' );


class recherry_icon_walker extends Walker_Nav_Menu {

    public function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

        global $wp_query;
        $indent = ( $depth ) ? str_repeat("\t", $depth) : '';

        $class_names = $value = '';
        $classes = empty($item->classes) ? array() : (array) $item->classes;
        $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item));
        $output .= $indent . '<li id="menu-item-' . $id . '"' . $value . $class_names . '>';

        $attributes = !empty($item->attr_title) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
        $attributes .=!empty($item->target) ? ' target="' . esc_attr($item->target) . '"' : '';
        $attributes .=!empty($item->xfn) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
        $attributes .=!empty($item->url) ? ' href="' . esc_attr($item->url) . '"' : '';

        if ($depth == 0) {
            $description = !empty($item->description) ? '<em>' . esc_attr($item->description) . '</em>' : '';
        } else {
            $description = "";
        }

        $item_output = $args->before;
        $item_output .= '<a' . $attributes . '>';
        $item_output .= '<i class="fa fa-' .  esc_attr($class_names) . '" aria-hidden="true"></i> ';
        $item_output .= $args->link_before . '<span class="icon-title">'. apply_filters('the_title', $item->title, $item->ID).'</span>';
        $item_output .= '</a>';
        $item_output .= $args->after;

        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
    }

}

/**
 * Enqueue scripts and styles.
 */
function redcherry_scripts() {
    wp_enqueue_style( 'redcherry-style', get_stylesheet_uri() );
    wp_enqueue_style( 'redcherry-font-awesome', get_template_directory_uri() . '/font-awesome/css/font-awesome.min.css' );

    wp_enqueue_script( 'redcherry-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

    wp_enqueue_script( 'redcherry-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'redcherry_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
