<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'redcherry_01');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '^$.: ;^G5I^YFV/fGqb6/9V`SO*|OVrdG,ug)KbgQj$6~K.u`qO;k^9*E}jM&{bz');
define('SECURE_AUTH_KEY',  'N**ExH8(tz*p--2g^UMY45@WtT.y}UB7HgX]3uOUPgIQS^Y6pY+9Rm~vf@xZ|_u}');
define('LOGGED_IN_KEY',    '%8#nuC3qQ[<?]b7ShMQ!=$08)I^_|f;q1`3`D2dH!Q|@|m>g Y1TfW4AhfEoBcw)');
define('NONCE_KEY',        'M7ULDbT#9+cl442Zdb&_b3Ye2d_%zP#rQ[ZI>Qa2r{ONXzZ7i(l`(l:okR61}M[s');
define('AUTH_SALT',        'iLmd=os=#n4|xP)h`4B50<(M*xDoG&b8kvzKr}uN_Od(Ah#!*^+g_jd[55J2_Q`[');
define('SECURE_AUTH_SALT', '5&rG%y;;.4B4-Be>K1Fpy(.g]]F8cAQGiNk.LX_eb>&7~!YAkbH6JRmxKmrj&f[)');
define('LOGGED_IN_SALT',   '6ttoeS@>z)Am/@mrv7{QLu!N3?pm{qwlfun^/@k((0REcxW:hRTo,cgsh[=|Ix=a');
define('NONCE_SALT',       'f#OgMP<n)Jt-3pR<;y`H_1kqtiGLz`zzPG8<.q3i$)CX]aA35cJ.w9H%uER>x!l+');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
